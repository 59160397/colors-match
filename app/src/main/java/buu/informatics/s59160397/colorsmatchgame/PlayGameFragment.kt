package buu.informatics.s59160397.colorsmatchgame

import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import buu.informatics.s59160397.colorsmatchgame.databinding.FragmentPlayGameBinding
import java.text.FieldPosition
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */


class PlayGameFragment : Fragment()  {
    //ถ้าไม่ประกาศแบบนี้ตัวแปรมันจะทับกันไปมา
    internal lateinit var iv_button: ImageView
    internal lateinit var iv_arrow:ImageView
    internal lateinit var tv_points: TextView
    internal lateinit var progressBar: ProgressBar
    internal lateinit var handler : Handler
    internal lateinit var runnable: Runnable
    var r : Random = Random()

    private val STATE_BLUE = 1
    private val STATE_RED = 2
    private val STATE_YELLOW = 3
    private val STATE_GREEN = 4
    var buttonState = STATE_BLUE
    var arrowState = STATE_BLUE

    var currentPoint = 0
    var currentTime = 4000
    var startTime = 4000


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentPlayGameBinding>(inflater, R.layout.fragment_play_game, container, false)
        setHasOptionsMenu(true)
        iv_button = binding.ivButton
        iv_arrow = binding.ivArrow
        progressBar = binding.progressBar
        tv_points = binding.tvPoints

        binding.ivButton.setOnClickListener {

            //rotate the button with the colors
            setButtonImage(setButtonPosition(buttonState))
            r = Random()
            buttonState = r.nextInt(4) + 1
            setButtonImage(buttonState)
        }

        handler = Handler()  //action จากการกดปุ่ม
        runnable = Runnable() {

            //show progress
            progressBar.setMax(startTime)
            progressBar.setProgress(startTime)
            currentTime = currentTime - 100
            progressBar.setProgress(currentTime)

            //check if there is still some time left in the progressbar
            if (currentTime > 0) {
                handler.postDelayed(runnable, 100)
            }else {

                //check if the colors of the arrow and the button are the same
                if (buttonState === arrowState){
                    //increase points and show them
                    currentPoint = currentPoint + 1
                    tv_points.setText("Points: ${currentPoint}")

                    //make the speed higher after every turn / if the speed make it again 2 second
                    startTime = startTime - 100
                    if (startTime < 1000) {
                        startTime = 2000
                    }
                    progressBar.setMax(startTime)
                    currentTime = startTime
                    progressBar.setProgress(currentTime)

                    //generate new color of the arrow
                    arrowState = r.nextInt(4)+1
                    setArrowImage(arrowState)

                    handler.postDelayed(runnable, 100)

                }
                else {
                    iv_button.setEnabled(false)
//Toast
                    Toast.makeText(context, "GAME OVER!" + "\nScore: $currentPoint", Toast.LENGTH_LONG).show()

                    //ส่งค่า Args score

                    handler.postDelayed({
                        //Navigate to the endgame
                        findNavController().navigate(PlayGameFragmentDirections.actionPlayGameFragmentToEndGameFragment(currentPoint))

                    }, 100)
                }
            }
        }
        //start the game loop
        handler.postDelayed(runnable, 100)
        return binding.root

    }

    private fun setArrowImage(state: Int) {
        when(state) {
            STATE_BLUE -> iv_arrow.setImageResource(R.drawable.ic_blue)
            STATE_RED -> iv_arrow.setImageResource(R.drawable.ic_red)
            STATE_YELLOW -> iv_arrow.setImageResource(R.drawable.ic_yellow)
            STATE_GREEN -> iv_arrow.setImageResource(R.drawable.ic_green)

        }
    }

    //rotate animation of the button when clicked
    private fun setRotation(image:ImageView, drawable:Int) {
        //rotate 90 degrees
        val rotateAnimation = RotateAnimation(0F, 90F, Animation.RELATIVE_TO_SELF, 0.5f,Animation.RELATIVE_TO_SELF, 0.5f)
        rotateAnimation.setDuration(100)
        rotateAnimation.setInterpolator(LinearInterpolator())
        rotateAnimation.setAnimationListener(object: Animation.AnimationListener {

            override fun onAnimationStart(p0: Animation?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onAnimationEnd(p0: Animation?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                image.setImageResource(drawable)
            }


            override fun onAnimationRepeat(p0: Animation?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

            }

        })
        image.startAnimation(rotateAnimation)
    }

    //set button colors position 1-4
    private fun setButtonPosition(position:Int ): Int {
        var position = position + 1
        if (position === 5) {
            position = 1
        }
        return position
    }
    //display the button color position
    private fun setButtonImage(state:Int) {
        when(state) {
            STATE_BLUE -> iv_button.setImageResource(R.drawable.ic_button_blue)
            STATE_RED -> iv_button.setImageResource(R.drawable.ic_button_red)
            STATE_YELLOW -> iv_button.setImageResource(R.drawable.ic_button_yellow)
            STATE_GREEN -> iv_button.setImageResource(R.drawable.ic_button_green)


        }
    }



}


