package buu.informatics.s59160397.colorsmatchgame


import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.NavigationUI
import buu.informatics.s59160397.colorsmatchgame.databinding.FragmentEndGameBinding
import kotlinx.android.synthetic.main.fragment_end_game.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val ARG_PARAM1 = "param1"
//private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class EndGameFragment : Fragment() {

    internal lateinit var point_over : TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentEndGameBinding>(inflater, R.layout.fragment_end_game, container, false)
       // รับค่า

        val args = EndGameFragmentArgs.fromBundle(arguments!!)
        args.scoreFinish

//       binding.pointOver

//        Toast.makeText(context, "Finish Score: ${args.scoreFinish}", Toast.LENGTH_LONG).show()

//        println()
        // Inflate the layout for this fragment

        setHasOptionsMenu(true)

        binding.pointOver.setText("Points: ${args.scoreFinish}")

        binding.playagainImage.setOnClickListener { view : View ->

            findNavController().navigate(EndGameFragmentDirections.actionEndGameFragmentToPlayGameFragment())

        }
        binding.homeImage.setOnClickListener { view: View ->
            findNavController().navigate(EndGameFragmentDirections.actionEndGameFragmentToHomeFragment())
        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.share_manu, menu)
        if (null == getShareIntent().resolveActivity(activity!!.packageManager)) {
            // hide the menu item if it doesn't resolve
            menu?.findItem(R.id.shareFragment)?.setVisible(false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item!!.itemId) {
            R.id.shareFragment -> shareSuccess()
        }
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }

    private fun getShareIntent() : Intent {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.setType("text/plain")
            .putExtra(Intent.EXTRA_TEXT,"Share")
        return shareIntent
    }

    private fun shareSuccess() {
        startActivity(getShareIntent())
    }



}
